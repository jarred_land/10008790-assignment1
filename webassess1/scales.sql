CREATE DATABASE Microtone;
Go

USE Microtone;
Go

CREATE TABLE ScaleTable (
    sclid INT NOT NULL, 
    Scale_Name VARCHAR(30),
    Scale_Description VARCHAR(255),
    Scale_Rating VARCHAR(10)
    FOREIGN KEY (authid) REFERENCES AuthorTable(authid)
); 

CREATE TABLE AuthorTable (
    authid INT NOT NULL, 
    Author VARCHAR(30),
    Scale_Produced VARCHAR(255),
    Contact_Information VARCHAR(100)
    FOREIGN KEY (sclid) REFERENCES ScaleTable(sclidid)
); 

INSERT INTO ScaleTable (sclid, Scale_Name, Scale_Description, Scale_Rating) VALUES ('1000', 'Bohlen-Pierce', 'Tritave scale based on dividing the octave into 13 1/2', '6')
INSERT INTO ScaleTable (sclid, Scale_Name, Scale_Description, Scale_Rating) VALUES ('1001', 'Cactus Rosary', 'Just intonation scale using 12 pre-defined pitches', '8')
INSERT INTO ScaleTable (sclid, Scale_Name, Scale_Description, Scale_Rating) VALUES ('1002', '24EDO', '24 equal divisions of the octave, resulting in 24 notes per octave rather than 12', '9')

INSERT INTO AuthorTable (authid, Author, Scale_Produced, Contact_Information) VALUES ('2000', 'Heinz Bohlen, John Pierce', 'Bohlen-Pierce', 'placeholder@email.com')
INSERT INTO AuthorTable (authid, Author, Scale_Produced, Contact_Information) VALUES ('2001', 'Terry Riley', 'Cactus Rosary', 'placeholder@email.com')
INSERT INTO AuthorTable (authid, Author, Scale_Produced, Contact_Information) VALUES ('2002', 'No Author', '24EDO', 'placeholder@email.com')